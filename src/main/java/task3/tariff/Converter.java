package task3.tariff;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Converter {

    private final static String baseFile = "collectionTariff.json";

    public static void toJSON(ListTariff listTariff) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(baseFile), listTariff);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("json created!");
    }

    public static ListTariff toJavaObject() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(baseFile), ListTariff.class);
    }

}
