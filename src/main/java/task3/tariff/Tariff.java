package task3.tariff;

import task3.enums.Rate;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlRootElement
@XmlType(name = "tariff", propOrder = {
        "name",
        "nameOperator",
        "payroll",
        "callPrices",
        "priceSMS",
        "growingParameters"})
public class Tariff {
    private String name;
    private String nameOperator;
    private int payroll;
    private CallPrices callPrices;
    private int priceSMS;
    private Parameters growingParameters;

    public Tariff(String name, String nameOperator, int payroll, int withinTheNetwork,
    int offGrid, int landlines, int priceSMS, int favoriteNumbers, Rate rate, int pay) {

        this.name = name;
        this.nameOperator = nameOperator;
        this.payroll = payroll;
        this.callPrices = new CallPrices(withinTheNetwork, offGrid, landlines);
        this.priceSMS = priceSMS;
        this.growingParameters = new Parameters(favoriteNumbers, rate, pay);
    }
    public Tariff() {
    }

    @XmlElement(name = "title")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameOperator() {
        return nameOperator;
    }

    public void setNameOperator(String nameOperator) {
        this.nameOperator = nameOperator;
    }

    public int getPayroll() {
        return payroll;
    }

    public void setPayroll(int payroll) {
        if(payroll < 0){
            payroll = 0;
        }
        this.payroll = payroll;
    }

    public CallPrices getCallPrices() {
        return callPrices;
    }

    public void setCallPrices(CallPrices callPrices) {
        this.callPrices = callPrices;
    }

    public int getPriceSMS() {
        return priceSMS;
    }

    public void setPriceSMS(int priceSMS) {
        if(priceSMS < 0){
            priceSMS = 0;
        }
        this.priceSMS = priceSMS;
    }

    public Parameters getGrowingParameters() {
        return growingParameters;
    }

    public void setGrowingParameters(Parameters growingParameters) {
        this.growingParameters = growingParameters;
    }

    @Override
    public String toString() {
        return new StringJoiner(" ")
                .add("name='" + name + "'")
                .add("nameOperator='" + nameOperator + "'")
                .add("payroll=" + payroll)
                .add("call=" + callPrices)
                .add("priceSMS=" + priceSMS)
                .add("growingParameters=" + growingParameters)
                .toString();
    }
}
