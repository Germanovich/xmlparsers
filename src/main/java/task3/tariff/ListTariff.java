package task3.tariff;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.StringJoiner;

@XmlRootElement(namespace = "list")
@XmlType(name = "tariffList")
public class ListTariff {

    @XmlElementWrapper(name = "gemsList")
    @XmlElement(name = "gem")
    private ArrayList<Tariff> tariffList;


    public ArrayList<Tariff> getTariffsList() {
        return this.tariffList;
    }

    public void setTariffList(ArrayList<Tariff> tariffList) {
        this.tariffList = tariffList;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ListTariff.class.getSimpleName() + "[", "]")
                .add("tariff=" + tariffList + "\n\n")
                .toString();
    }
}
