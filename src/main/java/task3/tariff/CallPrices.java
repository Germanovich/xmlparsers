package task3.tariff;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallPrices", propOrder = {
})
public class CallPrices {

    private int withinTheNetwork;
    private int offGrid;
    private int landlines;

    public CallPrices(int withinTheNetwork, int offGrid, int landlines) {
        this.withinTheNetwork = withinTheNetwork;
        this.offGrid = offGrid;
        this.landlines = landlines;
    }
    public CallPrices(){
    }

    public int getWithinTheNetwork() {
        return withinTheNetwork;
    }

    public void setWithinTheNetwork(int withinTheNetwork) {
        if(withinTheNetwork < 0){
            withinTheNetwork = 0;
        }
        this.withinTheNetwork = withinTheNetwork;
    }

    public int getOffGrid() {
        return offGrid;
    }

    public void setOffGrid(int offGrid) {
        if(offGrid < 0){
            offGrid = 0;
        }
        this.offGrid = offGrid;
    }

    public int getLandlines() {
        return landlines;
    }

    public void setLandlines(int landlines) {
        if(landlines < 0){
            landlines = 0;
        }
        this.landlines = landlines;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CallPrices.class.getSimpleName() + "[", "]")
                .add("withinTheNetwork=" + withinTheNetwork)
                .add("offGrid=" + offGrid)
                .add("landlines=" + landlines)
                .toString();
    }
}
