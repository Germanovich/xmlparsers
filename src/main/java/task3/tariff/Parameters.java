package task3.tariff;

import task3.enums.Rate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "parameters", propOrder = {
        "rate",
        "favoriteNumbers",
        "pay"
})
public class Parameters {
    private Rate rate;
    private int favoriteNumbers;
    private int pay;

    public Parameters(int favoriteNumbers, Rate rate, int pay) {
        this.rate = rate;
        this.favoriteNumbers = favoriteNumbers;
        this.pay = pay;
    }

    public Parameters() {
    }

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    public int getFavoriteNumbers() {
        return favoriteNumbers;
    }

    public void setFavoriteNumbers(int favoriteNumbers) {
        this.favoriteNumbers = favoriteNumbers;
    }

    public int getPay() {
        return pay;
    }

    public void setPay(int pay) {
        this.pay = pay;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ")
                .add("rate=" + rate)
                .add("favoriteNumbers=" + favoriteNumbers)
                .add("pay=" + pay)
                .toString();
    }
}
