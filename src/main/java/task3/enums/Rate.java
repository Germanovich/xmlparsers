package task3.enums;

public enum Rate {
    TWELVESECOND("12-second"),

    PER_MINUTE("Per minute");

    private final String value;

    Rate(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "'" + value + "'";
    }
}
