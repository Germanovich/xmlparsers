package task3;

import task3.enums.Rate;
import task3.tariff.Converter;
import task3.tariff.ListTariff;
import task3.tariff.Tariff;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class App {

    private static final String FLOWERS_XML = "./tariff-jaxb.xml";

    public static void main(String[] args) throws JAXBException, FileNotFoundException {
        ArrayList<Tariff> tariffArrayList = new ArrayList<>();

        tariffArrayList.add(new Tariff(
                "Лимон",
                "A1",
                25,
                2,
                3,
                5,
                1,
                5,
                Rate.PER_MINUTE,
                7));


        tariffArrayList.add(new Tariff(
                "Лимон+",
                "A1",
                50,
                4,
                6,
                10,
                2,
                10,
                Rate.TWELVESECOND,
                10));
        tariffArrayList.add(new Tariff(
                "Лимон-",
                "A1",
                15,
                3,
                4,
                3,
                1,
                2,
                Rate.PER_MINUTE,
                2));

        ListTariff listTariff = new ListTariff();
        listTariff.setTariffList(tariffArrayList);

        JAXBContext context = JAXBContext.newInstance(ListTariff.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Write to System.out
        m.marshal(listTariff, System.out);

        // Write to File
        m.marshal(listTariff, new File(FLOWERS_XML));

        System.out.println("\nOutput from our XML File: ");
        Unmarshaller um = context.createUnmarshaller();
        ListTariff listTariffLoad = (ListTariff) um.unmarshal(new FileReader(FLOWERS_XML));

        ArrayList<Tariff> list = listTariffLoad.getTariffsList();
        for (Tariff f : list) {
            System.out.println("Flower: " + f.toString());
        }
        Converter.toJSON(listTariffLoad);
    }
}
