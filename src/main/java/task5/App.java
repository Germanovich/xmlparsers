package task5;

import task5.enums.Group;
import task5.enums.Port;
import task5.flower.Converter;
import task5.flower.Device;
import task5.flower.ListDevice;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class App {

    private static final String FLOWERS_XML = "./devices-jaxb.xml";

    public static void main(String[] args) throws JAXBException, FileNotFoundException {
        ArrayList<Device> deviceArrayList = new ArrayList<>();

        deviceArrayList.add(new Device(
                "мышь",
                "Китай",
                21,
                20,
                true,
                false,
                Group.INPUT_OUTPUT,
                Port.USB,
                true
        ));

        deviceArrayList.add(new Device(
                "монитор",
                "Россия",
                214,
                220,
                true,
                false,
                Group.INPUT_OUTPUT,
                Port.USB,
                true
        ));

        deviceArrayList.add(new Device(
                "клавиатура",
                "Китай",
                49,
                26,
                true,
                false,
                Group.INPUT_OUTPUT,
                Port.USB,
                true
        ));
        ListDevice listDevice = new ListDevice();
        listDevice.setDeviceList(deviceArrayList);

        JAXBContext context = JAXBContext.newInstance(ListDevice.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Write to System.out
        m.marshal(listDevice, System.out);

        // Write to File
        m.marshal(listDevice, new File(FLOWERS_XML));

        System.out.println("\nOutput from our XML File: ");
        Unmarshaller um = context.createUnmarshaller();
        ListDevice listDeviceLoad = (ListDevice) um.unmarshal(new FileReader(FLOWERS_XML));

        ArrayList<Device> list = listDeviceLoad.getDevicesList();
        for (Device f : list) {
            System.out.println("Flower: " + f.toString());
        }
        Converter.toJSON(listDeviceLoad);
    }
}
