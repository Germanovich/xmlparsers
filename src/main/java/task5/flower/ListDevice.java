package task5.flower;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.StringJoiner;

@XmlRootElement(namespace = "list")
@XmlType(name = "deviceList")
public class ListDevice {

    @XmlElementWrapper(name = "devicesList")
    @XmlElement(name = "device")
    private ArrayList<Device> deviceList;


    public ArrayList<Device> getDevicesList() {
        return this.deviceList;
    }

    public void setDeviceList(ArrayList<Device> deviceList) {
        this.deviceList = deviceList;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ListDevice.class.getSimpleName() + "[", "]")
                .add("device=" + deviceList + "\n\n")
                .toString();
    }
}
