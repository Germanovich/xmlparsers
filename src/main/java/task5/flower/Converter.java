package task5.flower;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Converter {

    private final static String baseFile = "collectionDevice.json";

    public static void toJSON(ListDevice listDevice) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(baseFile), listDevice);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("json created!");
    }

    public static ListDevice toJavaObject() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(baseFile), ListDevice.class);
    }

}
