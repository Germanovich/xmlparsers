package task5.flower;

import task5.enums.Group;
import task5.enums.Port;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlRootElement
@XmlType(name = "device", propOrder = {
        "name",
        "origin",
        "price",
        "type",
        "critical"})
public class Device {
    private String name;
    private String origin;
    private int price;
    private Type type;
    private boolean critical;

    public Device(String name, String origin, int price, int energyConsumption, boolean peripheral, boolean cooler,
                  Group groupComponent, Port port, boolean critical) {
        this.name = name;
        this.origin = origin;
        this.price = price;
        this.type = new Type(energyConsumption, peripheral, cooler, groupComponent, port);
        this.critical = critical;
    }

    public Device() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        if(price < 0){
            price = 0;
        }
        this.price = price;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isCritical() {
        return critical;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Device.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("origin='" + origin + "'")
                .add("price=" + price + "$")
                .add("type=" + type)
                .add("critical=" + critical)
                .toString();
    }
}
