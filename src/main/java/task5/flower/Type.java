package task5.flower;

import task5.enums.Group;
import task5.enums.Port;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "type", propOrder = {
        "energyConsumption",
        "peripheral",
        "cooler",
        "groupComponent",
        "port",
})
public class Type {
    private int energyConsumption;
    private boolean peripheral;
    private boolean cooler;
    private Group groupComponent;
    private Port port;


    public Type(int energyConsumption, boolean peripheral, boolean cooler, Group groupComponent, Port port) {
        this.energyConsumption = energyConsumption;
        this.peripheral = peripheral;
        this.cooler = cooler;
        this.groupComponent = groupComponent;
        this.port = port;
    }

    public Type() {
    }

    public int getEnergyConsumption() {
        return energyConsumption;
    }

    public void setEnergyConsumption(int energyConsumption) {
        this.energyConsumption = energyConsumption;
    }

    public boolean isPeripheral() {
        return peripheral;
    }

    public void setPeripheral(boolean peripheral) {
        this.peripheral = peripheral;
    }

    public boolean isCooler() {
        return cooler;
    }

    public void setCooler(boolean cooler) {
        this.cooler = cooler;
    }

    public Group getGroupComponent() {
        return groupComponent;
    }

    public void setGroupComponent(Group groupComponent) {
        this.groupComponent = groupComponent;
    }

    public Port getPort() {
        return port;
    }

    public void setPort(Port port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Type.class.getSimpleName() + "[", "]")
                .add("energyConsumption=" + energyConsumption + "ватт")
                .add("peripheral=" + peripheral)
                .add("cooler=" + cooler)
                .add("groupComponent=" + groupComponent)
                .add("port=" + port)
                .toString();
    }
}
