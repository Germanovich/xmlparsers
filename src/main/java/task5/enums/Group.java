package task5.enums;

public enum Group {
    INPUT_OUTPUT("Input-output"),

    MULTIMEDIA("Multimedia");

    private final String value;

    Group(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "'" + value + "'";
    }
}
