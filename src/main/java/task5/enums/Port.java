package task5.enums;

public enum Port {
    COM("COM"),

    USB("USB"),

    LPT("LPT");

    private final String value;

    Port(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "'" + value + "'";
    }
}
