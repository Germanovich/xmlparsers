package File;

import java.io.*;
import java.util.*;

public class task1 {

    public static void main(String[] args) throws IOException {
        task1 test = new task1();
        test.writeFile();
    }

    private int[] array = new int[10];
    private ArrayList<Integer> arraySort = new ArrayList<>();
    File file = new File("Test.txt");

    public task1() throws IOException {
        file.createNewFile();
        createArray();
    }

    private void createArray() {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100) - 50;
        }
    }

    public void writeFile() throws IOException {
        createArray();
        FileWriter filewriter = new FileWriter(new File("Test.txt"));

        for (int i = 0; i < array.length; ++i) {
            filewriter.write(array[i] + " ");
        }
        filewriter.flush();
        readFile();
    }

    public void readFile() throws IOException {

        Scanner scannerFile = new Scanner(file);
        for (int i = 0; i < array.length; ++i) {
            if (scannerFile.hasNextInt())
                arraySort.add(scannerFile.nextInt());

        }
        Collections.sort(arraySort);

        System.out.print("Введенный массив\n");
        System.out.print(Arrays.toString(arraySort.toArray()));
    }
}
