package File;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class task2 {
    public static void main(String[] args) {
        String line = "Some data to be written and read.";

        String fileName1 = "file_1.txt";
        String fileName2 = "file_2.txt";

        writeFile(fileName1, "UTF16", line);

        try {
            newFile(fileName1, fileName2);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void writeFile(String fileName, String charsetName, String line) {

        try {
            BufferedWriter bw =
                    new BufferedWriter(
                            new OutputStreamWriter(
                                    new FileOutputStream(fileName), charsetName));

            System.out.println("Write some data to file: " + fileName);
            bw.write(line);
            bw.close();

            // Считываем результат
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);

            System.out.println("Read data from file: " + fileName);
            System.out.println("Read: " + br.readLine());

            br.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void newFile(String fileName1, String fileName2) throws IOException {
        RandomAccessFile raf = new RandomAccessFile(new File(fileName1), "r");
        String line = raf.readLine();
        line = new String(line.getBytes(StandardCharsets.UTF_16BE), StandardCharsets.UTF_8);
        raf.close();

        writeFile(fileName2, "UTF8", line);
    }
}
