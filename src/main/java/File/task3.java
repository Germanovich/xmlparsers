package File;

import java.io.*;
import java.util.Scanner;

public class task3 {
    public static void main(String[] args) throws FileNotFoundException {

        Scanner sc = new Scanner(new File("in.txt"));
        PrintWriter pw = new PrintWriter(new File("out.txt"));

        String[] student;

        while (sc.hasNextLine()) {
            student = sc.nextLine().split("\\s+");

            double sum = 0;

            for (int i = 1; i < student.length; i++) {
                sum += Integer.parseInt(student[i]);
            }
            if (sum / (student.length - 1) > 7) {
                pw.println(student[0].toUpperCase());
            }
        }

        sc.close();
        pw.close();

    }

}