package task4;

import task4.enums.Group;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlRootElement
@XmlType(name = "medicine")

public class Medicine {
    private String name;
    private String pharm;
    private Group group;
    private Analogs analogs;
    private Version version;

    public Medicine(String name, String pharm, Group group, Analogs analogs, Version version) {
        this.name = name;
        this.pharm = pharm;
        this.group = group;
        this.analogs = analogs;
        this.version = version;
    }

    public Medicine() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPharm() {
        return pharm;
    }

    public void setPharm(String pharm) {
        this.pharm = pharm;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Analogs getAnalogs() {
        return analogs;
    }

    public void setAnalogs(Analogs analogs) {
        this.analogs = analogs;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Medicine.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("pharm='" + pharm + "'")
                .add("group=" + group)
                .add("analogs=" + analogs)
                .add("versions=" + version)
                .toString();
    }
}
