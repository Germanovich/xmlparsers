package task4.enums;

public enum Consistence {
    TABLETS("Tablets"),

    PILLS("Pills"),

    POWDER("Powder");

    private final String value;

    Consistence(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "'" + value + "'";
    }
}
