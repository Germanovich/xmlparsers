package task4.enums;

public enum Group {
    ANTIBIOTIC("Antibiotic"),

    PAINKILLER("Painkiller"),

    VITAMIN("Vitamin");

    private final String value;

    Group(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "'" + value + "'";
    }
}
