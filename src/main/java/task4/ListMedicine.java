package task4;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.StringJoiner;

@XmlRootElement(namespace = "list")
@XmlType(name = "medicineList")
public class ListMedicine {

    @XmlElementWrapper(name = "medicinesList")
    @XmlElement(name = "medicine")
    private ArrayList<Medicine> medicineList;


    public ArrayList<Medicine> getMedicinesList() {
        return this.medicineList;
    }

    public void setMedicineList(ArrayList<Medicine> medicineList) {
        this.medicineList = medicineList;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ListMedicine.class.getSimpleName() + "[", "]")
                .add("tariff=" + medicineList + "\n\n")
                .toString();
    }
}
