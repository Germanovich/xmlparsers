package task4;

import task4.enums.Consistence;
import task4.produser.Producer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.StringJoiner;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "version", propOrder = {
        "consistence",
        "producers"
})
public class Version {
    private Consistence consistence;
    private ArrayList<Producer> producers;

    public Version(Consistence consistence, ArrayList<Producer> producers) {
        this.consistence = consistence;
        this.producers = producers;
    }

    public Version() {
    }

    public Consistence getConsistence() {
        return consistence;
    }

    public void setConsistence(Consistence consistence) {
        this.consistence = consistence;
    }

    public ArrayList<Producer> getProducers() {
        return producers;
    }

    public void setProducers(ArrayList<Producer> producers) {
        this.producers = producers;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Version.class.getSimpleName() + "[", "]")
                .add("consistence=" + consistence)
                .add("producers=" + producers)
                .toString();
    }
}
