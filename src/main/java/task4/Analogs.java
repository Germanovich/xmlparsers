package task4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.StringJoiner;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "analogs", propOrder = {
        "analogs"
})
public class Analogs {

    private ArrayList<String> analogs;

    public Analogs(){
    }

    public Analogs(ArrayList<String> analogs) {
        this.analogs = analogs;
    }

    public ArrayList<String> getAnalogs() {
        return analogs;
    }

    public void setAnalogs(ArrayList<String> analogs) {
        this.analogs = analogs;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Analogs.class.getSimpleName() + "[", "]")
                .add("analogs=" + analogs)
                .toString();
    }
}
