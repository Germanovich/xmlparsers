package task4;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Converter {

    private final static String baseFile = "collectionMedicine.json";

    public static void toJSON(ListMedicine listMedicine) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(baseFile), listMedicine);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("json created!");
    }

    public static ListMedicine toJavaObject() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(baseFile), ListMedicine.class);
    }

}
