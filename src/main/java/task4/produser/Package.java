package task4.produser;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Package", propOrder = {
        "type",
        "number",
        "price"})
public class Package {
    private String type;
    private int number;
    private int price;

    public Package(String type, int number, int price) {
        this.type = type;
        this.number = number;
        this.price = price;
    }

    public Package() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Package.class.getSimpleName() + "[", "]")
                .add("type='" + type + "'")
                .add("number=" + number)
                .add("price=" + price + "$")
                .toString();
    }
}
