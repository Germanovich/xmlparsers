package task4.produser;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Producer", propOrder = {
        "nameProducer",
        "certificate",
        "dosage",
        "aPackage"
})
public class Producer {
    private String nameProducer;
    private Certificate certificate;
    private Dosage dosage;
    private Package aPackage;

    public Producer(String nameProducer, Certificate certificate, Dosage dosage, Package aPackage) {
        this.nameProducer = nameProducer;
        this.certificate = certificate;
        this.dosage = dosage;
        this.aPackage = aPackage;
    }

    public Producer() {
    }

    public String getNameProducer() {
        return nameProducer;
    }

    public void setNameProducer(String nameProducer) {
        this.nameProducer = nameProducer;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public Dosage getDosage() {
        return dosage;
    }

    public void setDosage(Dosage dosage) {
        this.dosage = dosage;
    }

    public Package getaPackage() {
        return aPackage;
    }

    public void setaPackage(Package aPackage) {
        this.aPackage = aPackage;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Producer.class.getSimpleName() + "[", "]")
                .add("name='" + nameProducer + "'")
                .add("certificate=" + certificate)
                .add("dosage=" + dosage)
                .add("aPackage=" + aPackage)
                .toString();
    }

}
