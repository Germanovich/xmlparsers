package task4.produser;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Dosage", propOrder = {
        "dosage",
        "frequency"})

public class Dosage {
    private int dosage;
    private int  frequency;

    public Dosage(int dosage, int frequency) {
        this.dosage = dosage;
        this.frequency = frequency;
    }

    public Dosage() {
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Dosage.class.getSimpleName() + "[", "]")
                .add("dosage=" + dosage + "mg")
                .add("frequency=" + frequency)
                .toString();
    }
}
