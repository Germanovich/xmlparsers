package task4.produser;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.StringJoiner;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Certificate", propOrder = {
        "numberCertificate",
        "dateOfIssue",
        "expiration",
        "centralInformationCommission"
})
public class Certificate {
    private int numberCertificate;
    private GregorianCalendar dateOfIssue;
    private GregorianCalendar expiration;
    private String centralInformationCommission;

    public Certificate(int numberCertificate, GregorianCalendar dateOfIssue, GregorianCalendar expiration, String centralInformationCommission) {
        this.numberCertificate = numberCertificate;
        this.dateOfIssue = dateOfIssue;
        this.expiration = expiration;
        this.centralInformationCommission = centralInformationCommission;
    }

    public Certificate() {
    }


    public int getNumberCertificate() {
        return numberCertificate;
    }

    public void setNumberCertificate(int numberCertificate) {
        this.numberCertificate = numberCertificate;
    }

    public String getCentralInformationCommission() {
        return centralInformationCommission;
    }

    public void setCentralInformationCommission(String centralInformationCommission) {
        this.centralInformationCommission = centralInformationCommission;
    }

    public GregorianCalendar getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(GregorianCalendar dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public GregorianCalendar getExpiration() {
        return expiration;
    }

    public void setExpiration(GregorianCalendar expiration) {
        this.expiration = expiration;
    }

    @Override
    public String toString() {
        final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy MMMM dd HH:mm:ss");
        return new StringJoiner(", ", Certificate.class.getSimpleName() + "[", "]")
                .add("number=" + numberCertificate)
                .add("dateOfIssue=" + DATE_FORMAT.format(dateOfIssue.getTime()))
                .add("expiration=" + DATE_FORMAT.format(expiration.getTime()))
                .add("centralInformationCommission='" + centralInformationCommission + "'")
                .toString();
    }
}
