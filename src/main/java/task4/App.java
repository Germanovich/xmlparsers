package task4;

import task4.enums.Consistence;
import task4.enums.Group;
import task4.produser.Certificate;
import task4.produser.Dosage;
import task4.produser.Package;
import task4.produser.Producer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class App {

    private static final String FLOWERS_XML = "./medicine-jaxb.xml";

    public static void main(String[] args) throws JAXBException, FileNotFoundException {

        ArrayList<String> analogsList = new ArrayList<>();
        analogsList.add("Марифин");
        analogsList.add("Гудемин");
        Analogs analogs = new Analogs(analogsList);

        Certificate certificate = new Certificate(
                1247,
                new GregorianCalendar(2002, Calendar.MAY, 30),
                new GregorianCalendar(2021, Calendar.DECEMBER, 20),
                "ДокАптека");

        Dosage dosage = new Dosage(
                25,
                7);

        Package aPackage = new Package(
                "Пакет",
                30,
                27);

        Producer producer = new Producer(
                "мпортЭксмо",
                certificate,
                dosage,
                aPackage);

        ArrayList<Producer> producerArrayList = new ArrayList<>();
        producerArrayList.add(producer);

        Version version = new Version(Consistence.PILLS, producerArrayList);

        ArrayList<Medicine> medicineArrayList = new ArrayList<>();

        medicineArrayList.add(new Medicine(
                "Дормацел",
                "ГосМин",
                Group.VITAMIN,
                analogs,
                version
        ));

        medicineArrayList.add(new Medicine(
                "Гарманол",
                "ГосМоск",
                Group.VITAMIN,
                analogs,
                version
        ));

        ListMedicine listMedicine = new ListMedicine();
        listMedicine.setMedicineList(medicineArrayList);

        JAXBContext context = JAXBContext.newInstance(ListMedicine.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Write to System.out
        m.marshal(listMedicine, System.out);

        // Write to File
        m.marshal(listMedicine, new File(FLOWERS_XML));

        System.out.println("\nOutput from our XML File: ");
        Unmarshaller um = context.createUnmarshaller();
        ListMedicine listMedicineLoad = (ListMedicine) um.unmarshal(new FileReader(FLOWERS_XML));

        ArrayList<Medicine> list = listMedicineLoad.getMedicinesList();
        for (Medicine f : list) {
            System.out.println("Flower: " + f.toString());
        }
        Converter.toJSON(listMedicineLoad);
    }
}
