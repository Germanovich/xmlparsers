package task1.enums;

import java.util.StringJoiner;

public enum Multiplying {
    LEAVES("Leaves"),

    CUTTINGS("Cuttings"),

    SEED("Seed");

    private final String value;

    Multiplying(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "'" + value + "'";
    }
}
