package task1.enums;

import java.util.StringJoiner;

public enum Soil {
    PODZOLIC("Podzolic"),

    UNPAVED("Unpaved"),

    SOD_PODZOLIC("Sod-podzolic");

    private final String value;

    Soil(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "'" + value + "'";
    }
}
