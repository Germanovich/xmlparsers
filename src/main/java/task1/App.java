package task1;

import task1.enums.Multiplying;
import task1.enums.Soil;
import task1.flower.Converter;
import task1.flower.Flower;
import task1.flower.ListFlower;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;

public class App {

    private static final String FLOWERS_XML = "./flowers-jaxb.xml";

    public static void main(String[] args) throws JAXBException, FileNotFoundException {
        ArrayList<Flower> flowerArrayList = new ArrayList<>();

        flowerArrayList.add(new Flower(
                "Роза",
                Soil.PODZOLIC,
                "Франция",
                18,
                true,
                120,
                Multiplying.SEED));
        flowerArrayList.add(new Flower(
                "Ромашка",
                Soil.SOD_PODZOLIC,
                "Беларусь",
                17,
                true,
                84,
                Multiplying.SEED));
        flowerArrayList.add(new Flower(
                "Одуванчик",
                Soil.UNPAVED,
                "Украина",
                15,
                true,
                67,
                Multiplying.SEED));

        ListFlower listFlower = new ListFlower();
        listFlower.setFlowerList(flowerArrayList);

        JAXBContext context = JAXBContext.newInstance(ListFlower.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Write to System.out
        m.marshal(listFlower, System.out);

        // Write to File
        m.marshal(listFlower, new File(FLOWERS_XML));

        System.out.println("\nOutput from our XML File: ");
        Unmarshaller um = context.createUnmarshaller();
        ListFlower listFlowerLoad = (ListFlower) um.unmarshal(new FileReader(FLOWERS_XML));

        ArrayList<Flower> list = listFlowerLoad.getFlowersList();
        for (Flower f : list) {
            System.out.println("Flower: " + f.toString());
        }
        Converter.toJSON(listFlowerLoad);
    }
}
