package task1.flower;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.StringJoiner;

@XmlRootElement(namespace = "list")
@XmlType(name = "flowerList")
public class ListFlower {

    @XmlElementWrapper(name = "flowersList")
    @XmlElement(name = "flower")
    private ArrayList<Flower> flowerList;


    public ArrayList<Flower> getFlowersList() {
        return this.flowerList;
    }

    public void setFlowerList(ArrayList<Flower> flowerList) {
        this.flowerList = flowerList;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ListFlower.class.getSimpleName() + "[", "]")
                .add("flowers=" + flowerList + "\n\n")
                .toString();
    }
}
