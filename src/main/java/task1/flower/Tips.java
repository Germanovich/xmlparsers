package task1.flower;

import javax.xml.bind.annotation.*;
import java.util.StringJoiner;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "originsType", propOrder = {
        "temperature",
        "lighting",
        "watering"
})
public class Tips {
    private int temperature;
    private boolean lighting;
    private int watering;

    public Tips(int temperature, boolean lighting, int watering) {
        this.temperature = temperature;
        this.lighting = lighting;
        this.watering = watering;
    }

    public Tips() {
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public boolean isLighting() {
        return lighting;
    }

    public void setLighting(boolean lighting) {
        this.lighting = lighting;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ")
                .add("temperature=" + temperature)
                .add("lighting=" + lighting)
                .add("watering=" + watering)
                .toString();
    }
}
