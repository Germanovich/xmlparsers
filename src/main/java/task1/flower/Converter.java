package task1.flower;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Converter {

    private final static String baseFile = "collectionFlower.json";

    public static void toJSON(ListFlower listFlower) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(baseFile), listFlower);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("json created!");
    }

    public static ListFlower toJavaObject() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(baseFile), ListFlower.class);
    }

}
