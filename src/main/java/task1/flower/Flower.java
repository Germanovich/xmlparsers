package task1.flower;

import task1.enums.Multiplying;
import task1.enums.Soil;

import javax.xml.bind.annotation.*;
import java.util.StringJoiner;

@XmlRootElement
@XmlType(name = "flower", propOrder = {
        "name",
        "soil",
        "origin",
        "growingTips",
        "multiplying"})
public class Flower  {
    private String name;
    private Soil soil;
    private String origin;
    private Tips growingTips;
    private Multiplying multiplying;

    public Flower(String name, Soil soil, String origin, int temperature,
                  boolean lighting, int watering, Multiplying multiplying) {

        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.growingTips = new Tips(temperature, lighting, watering);
        this.multiplying = multiplying;
    }
    public Flower() {
    }

    @XmlElement(name = "title")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Soil getSoil() {
        return soil;
    }

    public void setSoil(Soil soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Tips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(Tips growingTips) {
        this.growingTips = growingTips;
    }

    public Multiplying getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(Multiplying multiplying) {
        this.multiplying = multiplying;
    }
    @Override
    public String toString() {
        return new StringJoiner(" ")
                .add("name='" + name + "'" + "\n")
                .add("soil=" + soil + "\n")
                .add("origin='" + origin + "'" + "\n")
                .add("growingTips=" + growingTips + "\n")
                .add("multiplying=" + multiplying + "\n")
                .toString();
    }

}
