package task2;

import task2.enums.Preciousness;
import task2.gem.Converter;
import task2.gem.Gem;
import task2.gem.ListGem;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class App {

    private static final String FLOWERS_XML = "./gem-jaxb.xml";

    public static void main(String[] args) throws JAXBException, FileNotFoundException {
        ArrayList<Gem> gemArrayList = new ArrayList<>();

        gemArrayList.add(new Gem(
                "Алмаз",
                Preciousness.PRECIOUS,
                "Рудник",
                "Голубой",
                85,
                7,
                12));

        gemArrayList.add(new Gem(
                "Рубин",
                Preciousness.PRECIOUS,
                "Саркофаг",
                "Красный",
                41,
                8,
                17));
        gemArrayList.add(new Gem(
                "Топаз",
                Preciousness.PRECIOUS,
                "Пещера",
                "Желтый",
                52,
                12,
                15));

        ListGem listGem = new ListGem();
        listGem.setGemList(gemArrayList);

        JAXBContext context = JAXBContext.newInstance(ListGem.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Write to System.out
        m.marshal(listGem, System.out);

        // Write to File
        m.marshal(listGem, new File(FLOWERS_XML));

        System.out.println("\nOutput from our XML File: ");
        Unmarshaller um = context.createUnmarshaller();
        ListGem listGemLoad = (ListGem) um.unmarshal(new FileReader(FLOWERS_XML));

        ArrayList<Gem> list = listGemLoad.getGemsList();
        for (Gem f : list) {
            System.out.println("Flower: " + f.toString());
        }
        Converter.toJSON(listGemLoad);
    }
}
