package task2.enums;

public enum Preciousness {
    PRECIOUS("Precious"),

    SEMIPRECIOUS("Semiprecious");

    private final String value;

    Preciousness(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "'" + value + "'";
    }
}
