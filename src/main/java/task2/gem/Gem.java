package task2.gem;

import task2.enums.Preciousness;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlRootElement
@XmlType(name = "gem", propOrder = {
        "name",
        "preciousness",
        "origin",
        "growingParameters",
        "carat"})
public class Gem {
    private String name;
    private Preciousness preciousness;
    private String origin;
    private Parameters growingParameters;
    private int carat;

    public Gem(String name, Preciousness preciousness, String origin, String color,
               int transparency, int cut, int carat) {

        this.name = name;
        this.preciousness = preciousness;
        this.origin = origin;
        this.growingParameters = new Parameters(color, transparency, cut);
        this.carat = carat;
    }
    public Gem() {
    }

    @XmlElement(name = "title")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Preciousness getPreciousness() {
        return preciousness;
    }

    public void setPreciousness(Preciousness preciousness) {
        this.preciousness = preciousness;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Parameters getGrowingParameters() {
        return growingParameters;
    }

    public void setGrowingParameters(Parameters growingParameters) {
        this.growingParameters = growingParameters;
    }

    public int getCarat() {
        return carat;
    }

    public void setCarat(int carat) {
        this.carat = carat;
    }

    @Override
    public String toString() {
        return new StringJoiner(" ")
                .add("name='" + name + "'")
                .add("soil=" + preciousness)
                .add("origin='" + origin + "'")
                .add("growingParameters=" + growingParameters)
                .add("carat=" + carat)
                .toString();
    }
}
