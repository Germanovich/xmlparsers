package task2.gem;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "originsType", propOrder = {
        "color",
        "cut",
        "transparency"
})
public class Parameters {
    private String color;
    private int cut;
    private int transparency;

    public Parameters(String color, int cut, int transparency) {
        this.color = color;
        this.cut = cut;
        this.transparency = transparency;
    }

    public Parameters() {
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCut() {
        return cut;
    }

    public void setCut(int cut) {
        if(cut < 4){
            cut = 4;

        } else if(cut > 15){
            cut = 15;
        }
        this.cut = cut;
    }

    public int getTransparency() {
        return transparency;
    }

    public void setTransparency(int transparency) {
        if(transparency < 0){
            transparency = 0;

        } else if(transparency > 100){
            transparency = 100;
        }
        this.transparency = transparency;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ")
                .add("color='" + color + "'")
                .add("cut=" + cut)
                .add("transparency=" + transparency + "%")
                .toString();
    }
}
