package task2.gem;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.StringJoiner;

@XmlRootElement(namespace = "list")
@XmlType(name = "gemList")
public class ListGem {

    @XmlElementWrapper(name = "gemsList")
    @XmlElement(name = "gem")
    private ArrayList<Gem> gemList;


    public ArrayList<Gem> getGemsList() {
        return this.gemList;
    }

    public void setGemList(ArrayList<Gem> gemList) {
        this.gemList = gemList;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ListGem.class.getSimpleName() + "[", "]")
                .add("gem=" + gemList + "\n\n")
                .toString();
    }
}
