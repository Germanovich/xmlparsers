package task6;

import task6.enums.Energy;
import task6.enums.Handy;
import task6.flower.Converter;
import task6.flower.PowerTool;
import task6.flower.ListPowerTool;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class App {

    private static final String FLOWERS_XML = "./powerTool-jaxb.xml";

    public static void main(String[] args) throws JAXBException, FileNotFoundException {
        ArrayList<PowerTool> powerToolArrayList = new ArrayList<>();

        powerToolArrayList.add(new PowerTool(
                "M347-5",
                Handy.ONE_HANDED,
                "США",
                Energy.AVERAGE,
                147,
                false,
                "металл"
        ));

        powerToolArrayList.add(new PowerTool(
                "M377-7",
                Handy.TWO_HANDED,
                "США",
                Energy.HIGH,
                250,
                true,
                "металл"
        ));

        powerToolArrayList.add(new PowerTool(
                "GB3-547",
                Handy.ONE_HANDED,
                "Китай",
                Energy.HIGH,
                162,
                false,
                "пластик"
        ));

        ListPowerTool listPowerTool = new ListPowerTool();
        listPowerTool.setPowerToolList(powerToolArrayList);

        JAXBContext context = JAXBContext.newInstance(ListPowerTool.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Write to System.out
        m.marshal(listPowerTool, System.out);

        // Write to File
        m.marshal(listPowerTool, new File(FLOWERS_XML));

        System.out.println("\nOutput from our XML File: ");
        Unmarshaller um = context.createUnmarshaller();
        ListPowerTool listPowerToolLoad = (ListPowerTool) um.unmarshal(new FileReader(FLOWERS_XML));

        ArrayList<PowerTool> list = listPowerToolLoad.getPowerToolsList();
        for (PowerTool f : list) {
            System.out.println("Flower: " + f.toString());
        }
        Converter.toJSON(listPowerToolLoad);
    }
}
