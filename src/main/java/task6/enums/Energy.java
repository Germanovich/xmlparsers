package task6.enums;

public enum Energy {
    LOW("Low"),

    AVERAGE("Average"),

    HIGH("High");

    private final String value;

    Energy(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "'" + value + "'";
    }
}
