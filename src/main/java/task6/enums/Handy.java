package task6.enums;

public enum Handy {
    ONE_HANDED("One-handed"),
    TWO_HANDED("Two-handed");

    private final String value;

    Handy(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "'" + value + "'";
    }
}
