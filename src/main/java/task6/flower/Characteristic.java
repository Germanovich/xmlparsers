package task6.flower;

import task6.enums.Energy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "characteristic", propOrder = {
        "energy",
        "productivity",
        "autonomous"
})
public class Characteristic {
    private Energy energy;
    private int productivity;
    private boolean autonomous;

    public Characteristic(Energy energy, int productivity, boolean autonomous) {
        this.energy = energy;
        this.productivity = productivity;
        this.autonomous = autonomous;
    }

    public Characteristic() {
    }

    public Energy getEnergy() {
        return energy;
    }

    public void setEnergy(Energy energy) {
        this.energy = energy;
    }

    public int getProductivity() {
        return productivity;
    }

    public void setProductivity(int productivity) {
        this.productivity = productivity;
    }

    public boolean isAutonomous() {
        return autonomous;
    }

    public void setAutonomous(boolean autonomous) {
        this.autonomous = autonomous;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Characteristic.class.getSimpleName() + "[", "]")
                .add("energy=" + energy)
                .add("productivity=" + productivity + "ед/час")
                .add("autonomous=" + autonomous)
                .toString();
    }
}
