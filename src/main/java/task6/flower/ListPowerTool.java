package task6.flower;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.StringJoiner;

@XmlRootElement(namespace = "list")
@XmlType(name = "powerToolList")
public class ListPowerTool {

    @XmlElementWrapper(name = "powerToolsList")
    @XmlElement(name = "powerTool")
    private ArrayList<PowerTool> powerToolList;


    public ArrayList<PowerTool> getPowerToolsList() {
        return this.powerToolList;
    }

    public void setPowerToolList(ArrayList<PowerTool> powerToolList) {
        this.powerToolList = powerToolList;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ListPowerTool.class.getSimpleName() + "[", "]")
                .add("powerTool=" + powerToolList + "\n\n")
                .toString();
    }
}
