package task6.flower;

import task6.enums.Energy;
import task6.enums.Handy;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlRootElement
@XmlType(name = "powerTool", propOrder = {
        "model",
        "handy",
        "origin",
        "growingCharacteristic",
        "material"})

public class PowerTool {
    private String model;
    private Handy handy;
    private String origin;
    private Characteristic growingCharacteristic;
    private String material;

    public PowerTool(String model, Handy handy, String origin, Energy energy,
                     int productivity, boolean autonomous, String material) {
        this.model = model;
        this.handy = handy;
        this.origin = origin;
        this.growingCharacteristic = new Characteristic(energy, productivity, autonomous);
        this.material = material;
    }

    public PowerTool() {
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Handy getHandy() {
        return handy;
    }

    public void setHandy(Handy handy) {
        this.handy = handy;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Characteristic getGrowingCharacteristic() {
        return growingCharacteristic;
    }

    public void setGrowingCharacteristic(Characteristic growingCharacteristic) {
        this.growingCharacteristic = growingCharacteristic;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PowerTool.class.getSimpleName() + "[", "]")
                .add("model='" + model + "'")
                .add("handy=" + handy)
                .add("origin='" + origin + "'")
                .add("growingCharacteristic=" + growingCharacteristic)
                .add("material='" + material + "'")
                .toString();
    }
}
